package com.ramonx86.app.gamepadtester.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ramonx86.app.gamepadtester.GamePadTester;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.vSyncEnabled = false;
		config.fullscreen = false;
		config.height = config.fullscreen ? LwjglApplicationConfiguration.getDesktopDisplayMode().height : 800;
		config.width = config.fullscreen ? LwjglApplicationConfiguration.getDesktopDisplayMode().width : 600;
		config.title = "GamePad Tester";
		new LwjglApplication(new GamePadTester(), config);
	}
}
