//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ramonx86.common.utility;

import java.lang.reflect.Field;

public class OS {
    public static final String name = System.getProperty("os.name").toLowerCase();
    public static final OS.Platform platform;
    public static String device;
    public static String manufacturer;
    public static String model;

    public OS() {
    }

    static {
        System.out.println("os.name: " + name);
        if(System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik")) {
            try {
                Class buildClass = Class.forName("android.os.Build");
                Field manufacturerField = buildClass.getDeclaredField("MANUFACTURER");
                Field modelField = buildClass.getDeclaredField("MODEL");
                Field deviceField = buildClass.getDeclaredField("DEVICE");
                Object m = manufacturerField.get((Object)null);
                if(m != null) {
                    manufacturer = m.toString().toLowerCase();
                    System.out.println("Manufacturer: " + manufacturer);
                } else {
                    manufacturer = "";
                }

                Object n = modelField.get((Object)null);
                if(n != null) {
                    model = n.toString().toLowerCase();
                    System.out.println("Model: " + model);
                } else {
                    model = "";
                }

                Object o = deviceField.get((Object)null);
                if(o != null) {
                    device = o.toString().toLowerCase();
                    System.out.println("Device: " + device);
                } else {
                    device = "";
                }
            } catch (Exception var7) {
                ;
            }

            if(device != null && device.contains("ouya")) {
                platform = OS.Platform.Ouya;
            } else {
                platform = OS.Platform.Android;
            }
        } else if(name.contains("linux")) {
            platform = OS.Platform.Linux;
        } else if(name.contains("win")) {
            platform = OS.Platform.Windows;
        } else if(name.contains("mac")) {
            platform = OS.Platform.Mac;
        } else if(!name.contains("nix") && !name.contains("nux") && !name.contains("aix")) {
            if(name.contains("sunos")) {
                platform = OS.Platform.Solaris;
            } else {
                platform = OS.Platform.Other;
            }
        } else {
            platform = OS.Platform.Unix;
        }

    }

    public static enum Platform {
        Windows,
        Mac,
        Unix,
        Solaris,
        Other,
        Android,
        Ouya,
        Linux;

        private Platform() {
        }
    }
}
