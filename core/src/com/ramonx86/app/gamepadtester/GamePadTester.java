package com.ramonx86.app.gamepadtester;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.ramonx86.common.utility.OS;

public class GamePadTester extends ApplicationAdapter
		implements InputProcessor, ControllerListener {

	protected Stage stage;
	protected BitmapFont font;
	protected Skin skin;
	protected SpriteBatch batch;
	protected FitViewport viewport;
	protected OrthographicCamera camera;

	protected Table guiMainLayout;
	protected List<String> listControllers;
	protected List<String> listInputMessages;
	protected List<String> listControllerEvents;
	float viewportWidth = 600;
	float viewportHeight = 800;
	protected Rectangle screenBounds;

	Array<String> arrayInputEvents = new Array<String>();
	Array<String> arrayControllerEvents = new Array<String>();
	Array<String> arrayControllerNames = new Array<String>();
	Array<Controller> controllers;

	int MAX_EVENT_COUNT = 8;
	float AXIS_THRESHOLD = 0.1f;//0.9f;//0.8f;//0.1f;

	int countExit = 0;
	final int[] specialKeys = {
			Input.Keys.ESCAPE,
			Input.Keys.BACK,
			Input.Keys.MENU
	};

	@Override
	public void create () {
		camera = new OrthographicCamera();
		camera.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		viewport = new FitViewport(viewportWidth,viewportHeight,camera);
		viewport.apply();

		batch = new SpriteBatch();
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"), new TextureAtlas("skin/uiskin.atlas"));
		font = skin.getFont("default-font");
		stage = new Stage(viewport);

		screenBounds = new Rectangle(0,0,viewportWidth,viewportHeight);

		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(this);
		multiplexer.addProcessor(stage);

		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
		Gdx.input.setInputProcessor(multiplexer);
		Controllers.addListener(this);
		controllers = Controllers.getControllers();

		initOSInfo();
		initGUI();
	}

	@Override
	public void dispose(){
		stage.dispose();
		skin.dispose();
		font.dispose();
	}

	@Override
	public void resize (int width, int height) {
		viewport.update(width, height);
		camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
	}

	private String initOSInfo(){
		StringBuilder sb = new StringBuilder();
		sb.append("Name: [" +OS.name+"] ");
		sb.append("\r\n");
		sb.append("Platform: [" +OS.platform+"] ");
		sb.append("\r\n");
		sb.append("Mfg: ["  +((OS.manufacturer != null) ? OS.manufacturer : "")+"] ");
		sb.append("\r\n");
		sb.append("Model: [" +((OS.model != null) ? OS.model : "")+"] ");
		sb.append("\r\n");
		sb.append("Device: ["+((OS.device != null) ? OS.device : "")+"] ");
		return sb.toString();
	}

	private void initGUI(){
		float lineHeight = font.getData().lineHeight * 1.6f;
		guiMainLayout = new Table();
		//guiMainLayout.setBounds(0,0,screenBounds.width,screenBounds.height);
		guiMainLayout.top().setFillParent(true);
		/* details of detected operating system  */
		guiMainLayout.add(new Label(initOSInfo(),skin)).left().top().expandX();
		guiMainLayout.row();
		/* list of detected controllers (on android this should get updated when a new controller is connected) */
		guiMainLayout.add(new Label("Controllers:",skin)).left().top().expandX().row();
		listControllers = new List<String>(skin);
		if (controllers != null) {
			for (Controller cc : controllers) {
				arrayControllerNames.add(cc.getName());
			}
		}
		listControllers.setItems(arrayControllerNames);
		guiMainLayout.add(listControllers).padLeft(20).height(lineHeight*4).left().prefWidth(viewportWidth);
		guiMainLayout.row();
		/* controller events detected  */
		guiMainLayout.add(new Label("Controller Events:",skin)).top().left().row();
		listControllerEvents = new List<String>(skin);
		listControllerEvents.setItems(arrayControllerEvents);
		guiMainLayout.add(listControllerEvents).padLeft(20).height(lineHeight*8).top().left().prefWidth(viewportWidth);
		guiMainLayout.row();
		/* other input events detected (touch/mouse events, keyboard keys, oher buttons) */
		guiMainLayout.add(new Label("Input Events:",skin)).top().left().row();
		listInputMessages = new List<String>(skin);
		listInputMessages.setItems(arrayInputEvents);
		guiMainLayout.add(listInputMessages).padLeft(20).height(lineHeight*8).top().left().prefWidth(viewportWidth);
		guiMainLayout.row().expand();

		guiMainLayout.setDebug(false);
		stage.addActor(guiMainLayout);
		guiMainLayout.setVisible(true);
	}

	@Override
	public void render () {
		stage.act(Gdx.graphics.getDeltaTime());
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		stage.draw();
		batch.end();
	}

/*  input processor methods BEGIN */

	/* convenience method to log an input event to the correct list */
	private void logInputEvent(String eventName, String valueOne, String valueTwo){
		StringBuilder sb = new StringBuilder();
		sb.append(eventName);
		if (valueOne != null) {
			sb.append("[").append(valueOne).append("]");
		}
		if (valueTwo != null) {
			sb.append(" : [").append(valueTwo).append("]");
		}
		arrayInputEvents.add(sb.toString());
		if (arrayInputEvents.size > MAX_EVENT_COUNT) {
			arrayInputEvents.removeIndex(0);
		}
		listInputMessages.setItems(arrayInputEvents);
		listInputMessages.setSelectedIndex(arrayInputEvents.size-1);
	}

	@Override
	public boolean keyDown(int keycode) {
		logInputEvent("keyDown", String.valueOf(keycode), Input.Keys.toString(keycode));
		if (isSpecialKey(keycode)) {
			countExit++;
		}
		if (countExit > 4) {
			Gdx.app.exit();
		}
		return false;
	}

	private boolean isSpecialKey(int keycode) {
		for (int i = 0; i < specialKeys.length; i++) {
			if (specialKeys[i] == keycode) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		logInputEvent("keyUp", String.valueOf(keycode), Input.Keys.toString(keycode));
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		logInputEvent("keyTyped",String.valueOf(character),null);
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		logInputEvent("touchDown",
				"x="+String.valueOf(screenX)+" y="+String.valueOf(screenY),
				"pointer="+String.valueOf(pointer)+" button="+String.valueOf(button));
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		logInputEvent("touchUp",
				"x="+String.valueOf(screenX)+" y="+String.valueOf(screenY),
				"pointer="+String.valueOf(pointer)+" button="+String.valueOf(button));
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		logInputEvent("touchDragged","x="+String.valueOf(screenX)+" y="+String.valueOf(screenY), "pointer="+String.valueOf(pointer));
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		logInputEvent("mouseMoved","x="+String.valueOf(screenX)+" y="+String.valueOf(screenY),null);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		logInputEvent("scrolled",String.valueOf(amount),null);
		return false;
	}
	/* input processor methods END  */
	/* controller listener methods START */
	private void logControllerEvent(String controllerName, String eventName, String valueOne, String valueTwo) {
		StringBuilder sb = new StringBuilder();
		sb.append(controllerName).append(": ").append(eventName);
		if (valueOne != null) {
			sb.append("[").append(valueOne).append("]");
		}
		if (valueTwo != null) {
			sb.append(" : [").append(valueTwo).append("]");
		}
		arrayControllerEvents.add(sb.toString());
		if (arrayControllerEvents.size > MAX_EVENT_COUNT) {
			arrayControllerEvents.removeIndex(0);
		}
		listControllerEvents.setItems(arrayControllerEvents);
		listControllerEvents.setSelectedIndex(arrayControllerEvents.size-1);
	}

	@Override
	public void connected(Controller controller) {
		logControllerEvent(controller.getName(),"connected",null,null);
		if (arrayControllerNames.contains(controller.getName(),false)){
			arrayControllerNames.add(controller.getName());
			listControllers.setItems(arrayControllerNames);
		}
	}

	@Override
	public void disconnected(Controller controller) {
		logControllerEvent(controller.getName(),"disconnected",null,null);
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		logControllerEvent(controller.getName(),"buttonDown",String.valueOf(buttonCode),Input.Keys.toString(buttonCode));
		return false;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		logControllerEvent(controller.getName(),"buttonUp",String.valueOf(buttonCode),Input.Keys.toString(buttonCode));
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		if (Math.abs(value) < AXIS_THRESHOLD) { return false; }
		logControllerEvent(controller.getName(),"axisMoved",String.valueOf(axisCode),String.valueOf(value));
		return false;
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection value) {
		logControllerEvent(controller.getName(),"povMoved",String.valueOf(povCode),value.name());
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		logControllerEvent(controller.getName(),"xSliderMoved",String.valueOf(sliderCode),String.valueOf(value));
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		logControllerEvent(controller.getName(),"ySliderMoved",String.valueOf(sliderCode),String.valueOf(value));
		return false;
	}

	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		logControllerEvent(controller.getName(),"accelerometerMoved",String.valueOf(accelerometerCode),value.toString());
		return false;
	}
	/*  controller listener methods END  */
}
