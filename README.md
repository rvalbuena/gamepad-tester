# README #

* Libgdx-based tester to display detected OS info and input/controller events to help create input mappings
* Version 1.1
* Added display of human-readable description for keys (using Gdx.Input.Keys.toString(keycode)
* Catching BACK and MENU keys prevented app from closing on some devices, press BACK or MENU 5x to exit

### How do I get set up? ###

* Import into Android Studio as a Gradle project

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact